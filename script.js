class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.cardElement = document.createElement('div');
        this.cardElement.className = 'card';
        this.render();
    }

    deletePost() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE'
        }).then(res => {
            if (res.ok) {
                this.cardElement.remove();
            } else {
                console.error('Failed to delete post');
            }
        }).catch(err => {
            console.error('Error deleting post:', err);
        });
    }

    render() {
        this.cardElement.innerHTML = `
            <div class="title">${this.post.title}</div>
            <div class="content">${this.post.body}</div>
            <div class="user">${this.user.name} ${this.user.surname} | ${this.user.email}</div>
            <div class="delete-btn">Delete</div>
        `;
        document.getElementById('feed').appendChild(this.cardElement);
        const deleteBtn = this.cardElement.querySelector('.delete-btn');
        deleteBtn.addEventListener('click', () => this.deletePost());
    }
}

window.onload = function() {
    Promise.all([
        fetch('https://ajax.test-danit.com/api/json/users').then(res => res.json()),
        fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json())
    ]).then(([users, posts]) => {
        posts.forEach(post => {
            let user = users.find(u => u.id === post.userId);
            if (user) {
                new Card(post, user);
            }
        });
    });
};
